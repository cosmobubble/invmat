/* Use the BLAS matrix inversion routine from the optimised ATLAS distribution. */
// Based on: http://stackoverflow.com/questions/3519959/computing-the-inverse-of-a-matrix-using-lapack-in-c

#include <stdlib.h>
#include <stdio.h>
#include "invmat.h"

// LU decomoposition of a general matrix
//void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

// Matrix inverse given LU decomposition
//void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);

void inverse(double* A, int N){
  int *IPIV = (int*) malloc((N+1)*sizeof(int));
  int LWORK = N*N;
  double *WORK = (double*) malloc(LWORK*sizeof(double));
  int INFO;

  // Invert matrix using BLAS routines
  dgetrf_(&N, &N, A, &N, IPIV, &INFO);
  dgetri_(&N, A, &N, IPIV, WORK, &LWORK, &INFO);
  
  free(IPIV); free(WORK);
}
