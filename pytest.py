#!/usr/bin/python
"""Test interface with BLAS/ATLAS library."""

import ctypes
import numpy as np
import cProfile

np.random.seed(10)

# Load matrix inversion library and set up function
invmat = ctypes.cdll.LoadLibrary("libinvmat.so")
invert = invmat.inverse
invert.argtypes = [np.ctypeslib.ndpointer(dtype=np.double), ctypes.c_int]
invert.restype = ctypes.c_void_p  #  Declare result type, same below.

# Create random symmetric matrix
N = 5000
m = np.random.normal(size=(N,N))
m = 0.5 * (m + m.T)

# (1) Standard numpy invert
print "\n (1) Numpy routine"
cProfile.run("""
minv1 = np.linalg.inv(m)
""")

# (2) Invert matrix using BLAS routine
print "\n (2) BLAS routine"
cProfile.run("""
minv = m.flatten() # Get flattened copy
invert(minv, N)
minv = minv.reshape((N,N))
""")

print ""
print np.dot(m, minv)
