/* Invert a matrix using the BLAS/ATLAS LU decomposition inversion routine. */

// LU decomoposition of a general matrix
void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

// Matrix inverse given LU decomposition
void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);

// Function to handle inversion
void inverse(double* A, int N);
