# Build matrix inversion library which uses BLAS/ATLAS optimised LU-Decomp. inversion routine

# Directories in which GSL, ATLAS can be found
#GSLDIR := "/home/bullp/postgrad/lib/libgsl15"
ATLASDIR := "/home/bullp/postgrad/lib/atlas"
THISDIR := "/home/bullp/postgrad/invm"

# Compile shared library (Mac OS X)
# Need to use 'install_name' instead of 'soname' due to some bug
dynlib_mac: clean invmat.o
	gcc -shared -L$(ATLASDIR)/lib -Wl,-install_name,libinvmat.so -o libinvmat.so invmat.o -lcblas -latlas -lm

# Compile shared library (Linux)
dynlib_linux: clean invmat.o
	gcc -shared -L$(ATLASDIR)/lib -Wl,-soname,libinvmat.so -o libinvmat.so invmat.o -lcblas -latlas -lm

# Compile static library
staticlib: clean invmat.o
	ar rcs libinvmat.a invmat.o

# Build object files
invmat.o:
	gcc -I$(ATLASDIR)/include -c -fPIC -O3 invmat.c -o invmat.o

test:
	gcc -c -O3 test.c -o test.o
	gcc -L$(ATLASDIR) -L$(THISDIR) test.o -o test -linvmat -latlas

# Remove built files
clean:
	rm -f invmat invmat.o libinvmat.so

