#include "invmat.h"
#include <stdio.h>

int main(void){

  int N = 2;
  double m[2*2] = { 1, 2, 3, 4 };
  int i, j;

  // Output the input matrix
  printf("\nINPUT MATRIX\n");
  for(i=0; i<N; i++){
    for(j=0; j<N; j++){
      printf("%f  ", m[N*i + j]);
    } // end j loop
    printf("\n");
  } // end i loop
  
  // Perform inversion of test matrix
  inverse(m, N);
  
  // Output the inverted matrix
  printf("\nINVERTED MATRIX\n");
  for(i=0; i<N; i++){
    for(j=0; j<N; j++){
      printf("%f  ", m[N*i+j]);
    } // end j loop
    printf("\n");
  } // end i loop

}
